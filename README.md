#### Doc文档
让天下没有难看的文档

![](http://upload-images.jianshu.io/upload_images/1229087-be382351995e948e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


#### 准备工作
>1此插件需要一般的翻墙,蓝灯/赛风等都可以,因为是发布在国外Packagist下的

#### git项目地址:
    Packagist: https://packagist.org/packages/aliang/doc
    git码云: https://git.oschina.net/mrmiao/DocSite
(内含使用文档)

#### 安装
根目录执行:

    composer require aliang/doc

如果已经安装过,需要更新的话执行:

    composer update aliang/doc

config/app.php添加服务者

    ALiang\Doc\DocServiceProvider::class,//5.1以上版本  
    
    "ALiang\Doc\DocServiceProvider",//5.0版本  

config/app.php添加门面

     'Doc' => ALiang\Doc\Facades\Doc::class,//5.1以上版本
     
     'Doc' => 'ALiang\Doc\Facades\Doc',//5.0以上版本

生成配置文件

    php artisan vendor:publish

检查public下是否多了md文件夹;  
检查config下是否多了doc.php配置;  
检查resources/views下是否多了vendor/doc文件夹;  

#### 使用
控制器中

        use Doc;//顶部门面
        
        public function index(Request $request)
        {
            return Doc::render();//渲染模板
        }

#### 模板样式修改
修改resources/views/doc/doc.balde.php即可

#### 添加文档
在public下新增test.md
在config/doc.php添加配置

        [
            "id" => 3,
            "uid" => "server",
            "title" => "2",
            "list" => [
                [
                    "id" => 4,
                    "uid" => "server1",
                    "title" => "2.1"
                ],
                [
                    "id" => 4,
                    "uid" => "test",
                    "title" => "2.2测试"
                ]
            ]
        ]

刷新页面即可显示,剩下的用在线md编辑器,或者客户端md编辑器编写文件吧

#### 修改文件类型和目录
修改config/doc.php即可

    'extension' => ".md",//文件后缀
    'doc_path' => "/md/",//public下文件目录


#### 注意
文件都是get请求获取的,且都是公开的,暂不支持私密文件;

#### 都不容易,喜欢就打赏下吧,谢谢啦

![](http://upload-images.jianshu.io/upload_images/1229087-eaa8b7804b1f62af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)