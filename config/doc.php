<?php
return [
    'extension' => ".md",//文件后缀
    'doc_path' => "/md/",//public下文件目录
    'menus' => [
        [
            "id" => 1,
            "uid" => "start",//文件名称
            "title" => "1",//列表显示名称
            "list" => [
                [
                    "id" => 2,
                    "uid" => "start1",
                    "title" => "1.1"
                ],
                [
                    "id" => 5,
                    "uid" => "start2",
                    "title" => "1.2"
                ]
            ]
        ],
        [
            "id" => 3,
            "uid" => "server",
            "title" => "2",
            "list" => [
                [
                    "id" => 4,
                    "uid" => "server1",
                    "title" => "2.1"
                ]
            ]
        ]
    ]
];