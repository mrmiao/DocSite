<?php
namespace ALiang\Doc\Facades;

use Illuminate\Support\Facades\Facade;

class Doc extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'doc';
    }
}