<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>文档</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <!--markdown.js-->
    <script src="http://fexteam.gz01.bdysite.com/ueditor/plugins/markdown.js"></script>
    <script src="https://cdn.bootcss.com/pagedown/1.0/Markdown.Converter.js"></script>
    <!--jquery and bootstrap-->
    <script src="http://fexteam.gz01.bdysite.com/ueditor/plugins/jquery-2.0.3.min.js"></script>
    <link rel="stylesheet" href="http://fexteam.gz01.bdysite.com/ueditor/plugins/bootstrap/css/bootstrap.min.css"/>
    <script src="http://fexteam.gz01.bdysite.com/ueditor/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!--code highlight-->
    <script src="http://fexteam.gz01.bdysite.com/ueditor/plugins/prettify/prettify.js" type="text/javascript"></script>
    <link rel="stylesheet" href="http://fexteam.gz01.bdysite.com/ueditor/plugins/prettify/prettify.css">
    <!--doc css-->
    <link rel="stylesheet" href="http://fexteam.gz01.bdysite.com/ueditor/css/doc.css"/>

    <script src="http://fexteam.gz01.bdysite.com/ueditor/js/beautify.js"></script>
    <script src="http://fexteam.gz01.bdysite.com/ueditor/js/beautify-css.js"></script>
    <script src="http://fexteam.gz01.bdysite.com/ueditor/js/beautify-html.js"></script>
</head>
<body>
{{--头部--}}
<div class="navbar navbar-inverse navbar-fixed-top ue-docs-nav" role="banner">
    <div class="container">
        <div class="navbar-header">
            <a href="JavaScript:;" class="navbar-brand">Docs</a>
        </div>
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul class="nav navbar-nav">
                <li>
                    <a href="JavaScript:;">演示</a>
                </li>
                <li>
                    <a href="JavaScript:;">官网</a>
                </li>
                <li>
                    <a href="JavaScript:;">论坛</a>
                </li>
            </ul>
        </nav>
    </div>
</div>

{{--顶部--}}
<div class="ue-docs-caption">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Doc文档</h1>
                <p>让天下没有难看的文档!</p>
            </div>
        </div>
    </div>
</div>

{{--列表--}}
<div class="container ue-docs-container">
    <div class="row">
        {{--文档列表--}}
        <div class="col-md-3">
            <div id="guidebar" class="ue-sidebar hidden-print" role="complementary">
                <ul class="nav ue-sidenav">
                    @if(!empty($doc['menus']))
                        @foreach($doc['menus'] as $k=>$v)
                            <li class="category"><a href="javascript:;">{{$v['title']}}</a>
                                @if(!empty($v['list']))
                                    <ul class="nav">
                                        @foreach($v['list'] as $k1=>$v1)
                                            <li>
                                                <a href="JavaScript:;" data-uid="{{$v1['uid']}}">{{$v1['title']}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
        {{--文档显示--}}
        <div class="col-md-9 ue-docs-main" role="main">
            <div id="show">
            </div>
        </div>
    </div>
</div>

{{--版权--}}
<div class="ue-footer" role="contentinfo">
    <div class="container">
        <ul class="footer-links">
            <li>Docs</li>
            <li class="muted">·</li>
            <li>Version 1.0</li>
            <li class="muted">·</li>
            <li><a>阿亮:1039803717@qq.com</a></li>
        </ul>
    </div>
</div>

<script>
    $(function () {
        $("#guidebar a").click(function () {
            var mdId = $(this).data("uid");
            if (mdId == "" || typeof(mdId) == "undefined") {
                return;
            }
            $("#guidebar li").removeClass("active");
            $(this).parent().addClass("active");
            $.ajax({
                type: "GET",
                url: "{{$doc["doc_path"]}}" + mdId + "{{$doc["extension"]}}",
                data: "",
                dataType: "html",
                success: function (data) {
//                    console.log(data);
                    var converter = new Markdown.Converter();
                    var htm = converter.makeHtml(data);
                    $("#show").html(htm);
                }
            });
        });
    });
</script>
</body>
</html>