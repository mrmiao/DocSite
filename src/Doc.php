<?php
namespace ALiang\Doc;

use Illuminate\Session\SessionManager;
use Illuminate\Config\Repository;

class Doc
{
    protected $session;
    protected $config;

    public function __construct(SessionManager $session, Repository $config)
    {
        $this->session = $session;
        $this->config = $config;
    }

    //渲染模板
    public function render()
    {
        $doc = $this->config["doc"];
        return view('Doc::doc')->with([
            "doc" => $doc
        ]);
    }

    public function test()
    {
        echo "test";
    }
}

