<?php

namespace ALiang\Doc;

use Illuminate\Support\ServiceProvider;

class DocServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'Doc');

        $this->publishes([
            __DIR__ . '/views' => base_path('resources/views/vendor/doc'),
            __DIR__ . '/../config/doc.php' => config_path('doc.php'),
            __DIR__ . '/md' => public_path('md'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app['doc'] = $this->app->share(function ($app) {
            return new Doc($app['session'], $app['config']);
        });
    }

    public function provides()
    {
        return ['doc'];
    }
}
